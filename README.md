# Summary
The application uses OpenTok library to allow the video chat with each other. Users can login/signup to use the application.

# Technologies Used
Stack View,
Auto Layout,
UIKit,
Swift,
Text Field Delegate,
Firebase Database, 
Firebase Auth, 
OpenTok, 
Firebase Cloud Messaging, 
Push notifications.

# User Interface
VideoChat has three main view controllers :
LoginViewController: The VC allows user to either signup or login to use the application

UserListViewController: The VC lists all the other users registered in the application and their availability status.

VideoChatViewController: : The VC allows users to videochat with each other





