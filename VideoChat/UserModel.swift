//
//  UserModel.swift
//  VideoChat
//
//  Created by Vidushi Jaiswal on 25/11/18.
//  Copyright © 2018 Vidushi Jaiswal. All rights reserved.
//

import Foundation

struct UserModel {
    var name: String
    var isOnline: Bool
    var email: String
    
    init(name: String, isOnline: Bool, email: String) {
        self.name = name
        self.isOnline = isOnline
        self.email = email
    }
    
}
