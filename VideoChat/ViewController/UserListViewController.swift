//
//  UserListViewController.swift
//  VideoChat
//
//  Created by Vidushi Jaiswal on 25/11/18.
//  Copyright © 2018 Vidushi Jaiswal. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import UserNotifications

class UserListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    
    //MARK: Properties
    var ref: DatabaseReference!
    var dbHandle: DatabaseHandle!
    
    @IBOutlet weak var tableView: UITableView!
   
    @IBOutlet weak var loggedInAsLabel: UILabel!
    let listCellIdentifier = "UserListCell"
    
    // Replace with your OpenTok API key
    var kApiKey = ""
    // Replace with your generated session ID
    var kSessionId = ""
    // Replace with your generated token
    var kToken = ""
    
    var users: [[String : Any]] = []
    var receiverId = ""
    var senderId = ""
    
    
    //MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: listCellIdentifier, bundle: nil), forCellReuseIdentifier: listCellIdentifier)
        NotificationCenter.default.addObserver(self, selector: #selector(self.inviteToCallButtonPressed(_:)), name: Notification.Name(Constants.NotificationObservers.inviteToCallButton), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.showAlert(_:)), name: Notification.Name(Constants.NotificationObservers.showAlert), object: nil)

          NotificationCenter.default.addObserver(self, selector: #selector(self.reloadData), name: Notification.Name(Constants.NotificationObservers.reloadData), object: nil)
//        fetchAllUsers()

        fetchAllUsers()
        refreshData()
        
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    //MARK: Notifications functions
    @objc func reloadData() {
        tableView.reloadData()
    }
    
    
    @objc func inviteToCallButtonPressed(_ notification: NSNotification)  {
        if let sender = notification.userInfo![Constants.FCMData.sender]  {
            let buttonPosition:CGPoint = (sender as AnyObject).convert(CGPoint.zero, to:self.tableView)
            let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
            let cell = tableView.cellForRow(at: indexPath!) as! UserListCell
            print("cell: \(String(describing: cell.userNameLabel.text))")
            self.getId(name: cell.userNameLabel.text!)
            for user in self.users {
                if (user[Constants.FCMData.userName] as! String) == cell.userNameLabel.text {
                    if user[Constants.FCMData.isAvailable] as! Bool == true {
                        print("SEND INVITE TO: \(String(describing: user[Constants.FCMData.deviceToken])) \(String(describing: user[Constants.FCMData.userName])))")
                        self.createSessionKeys(user[Constants.FCMData.deviceToken] as! String)
                    } else {
                        let alertController = UIAlertController(title: "User is busy", message: "Try again after sometime", preferredStyle: .alert)
                        
                        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                        alertController.addAction(defaultAction)
                        
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                }
            }
            
        }
    }
    
    
    
    @objc func showAlert(_ notification: NSNotification) {
        if let sender = notification.userInfo![Constants.FCMData.name] as? String {
            let alert = UIAlertController(title: "Alert", message: "\(self.getDisplayName(email: sender)) invited you to video chat", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: {action in
                self.moveToVideoChat(kApiKey: (notification.userInfo![Constants.FCMData.kApiKey] as? String)!, kSessionId: (notification.userInfo![Constants.FCMData.kSessionId] as? String)!, kToken: (notification.userInfo![Constants.FCMData.kToken] as? String)!)
                
            }))
            alert.addAction(UIAlertAction(title: "Nope", style: .cancel, handler: { action in
                print("Show decline alert")
                
                self.sendDeclineNotification(token: (notification.userInfo![Constants.FCMData.fromDevice] as? String)!)
                
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: Helper Functions
    func fetchAllUsers() {
        self.users.removeAll()
        let currentUserEmail = Auth.auth().currentUser?.email
        ref = Database.database().reference()
        dbHandle = ref.child(Constants.DataBase.users).observe(.childAdded, with: {(data) in
            print(data)
           
            let value = data.value as! [String : Any]
            if value["userEmail"] as? String != currentUserEmail  {
                self.users.append(value)
            }
            print("USERS: \(self.users)")
            if Auth.auth().currentUser != nil {
                self.loggedInAsLabel.text = "Logged In as: \(self.getDisplayName(email: (Auth.auth().currentUser?.email)!))"
            }
            self.tableView.reloadData()
            
            
        })
    }

    func refreshData() {
        let connectedRef = Database.database().reference()
        connectedRef.observe(.childChanged, with: { (snapshot) -> Void in
            print("value changed: refresh table view")
            self.fetchAllUsers()
        })
    }
    

    func getDisplayName(email: String) -> String {
        for user in  self.users  {
            if user[Constants.DataBase.userEmail] as! String == email {
                return user[Constants.DataBase.userName] as! String
            }
        }
        return email
    }
    
    func getId(name: String)  {
        
        
        ref?.child(Constants.DataBase.users).observeSingleEvent(of: .value, with: {(snapshot) in
            for snap in snapshot.children.allObjects as! [DataSnapshot] {
                let value = snap.value as! [String : Any]
                if value[Constants.DataBase.userName] as? String == name {
                    print("Key: \(snap.key)")
                    
                }
            }
            print("sender id: \(Auth.auth().currentUser?.uid)")
            
        })
    }
    

    
    func moveToVideoChat(kApiKey: String, kSessionId: String, kToken: String) {
        
//        //change status of sender and receiver
        
        FCMTokenHelper.sharedInstance.updateIsAvailableStatus(token: (Auth.auth().currentUser?.uid)!, value: false)

        
        print("KEYS:: \(kApiKey)    \(kSessionId)   \(kToken)")
        let videoVC = storyboard!.instantiateViewController(withIdentifier: "VideoCallViewController") as! VideoCallViewController
        videoVC.modalTransitionStyle = .crossDissolve
        videoVC.kApiKey = kApiKey
        videoVC.kSessionId = kSessionId
        videoVC.kToken = kToken
        self.present(videoVC, animated: false, completion: nil)
    }
    
    func createSessionKeys(_ deviceToken: String) {
        
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        let url = URL(string: "https://videochat07.herokuapp.com/session")
        let dataTask = session.dataTask(with: url!) {
            (data: Data?, response: URLResponse?, error: Error?) in
            
            guard error == nil, let data = data else {
                print(error!)
                return
            }
            
            let dict = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [AnyHashable: Any]
             self.kApiKey = dict?[Constants.FCMData.apiKey] as? String ?? ""
             self.kSessionId = dict?[Constants.FCMData.sessionId] as? String ?? ""
             self.kToken = dict?[Constants.FCMData.token] as? String ?? ""
//         self.sendInviteTo(token: (user["deviceToken"] as? String)!)
             self.sendInviteTo(token: deviceToken)
            DispatchQueue.main.async {
                self.moveToVideoChat(kApiKey: self.kApiKey, kSessionId: self.kSessionId, kToken: self.kToken)
            }
      }
        dataTask.resume()
//        session.finishTasksAndInvalidate()
    }
    
    func sendDeclineNotification(token: String) {
        print("Decline : \(token)")
        let url = URL(string: "https://fcm.googleapis.com/fcm/send")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=AAAAhcXRT-w:APA91bHWuRt8nzaGQFEVjrWrXJiAMKIeia6epWY09gjVP3_o2NRy38KXmROqY_TR1o6F0loJYbOE8EM6siN2zFPcq_Z5R0RI4arXAgnY_SPYAsf5mUInBo5Hv4NDbTmVegXMwGIVrHEY", forHTTPHeaderField: "Authorization")
        request.httpMethod = "POST"
        
        var notData: [String: Any] = [
            "to" : token,
            "notification": [
                "title" : "Request Declines",
                "body"  : "User not availbale. Please try again after some time.",
                //                icon  : "not icon"
            ],
            "data": [
                //More notification data.
                Constants.FCMData.notificationName: Constants.FCMData.decline,
                Constants.FCMData.name: "\(self.getDisplayName(email: (Auth.auth().currentUser?.email)!))",
            ]
        ]
        
        request.httpBody = try? JSONSerialization.data(withJSONObject: notData, options: [])
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(responseString)")
        }
        task.resume()
        
    }
    
  
    
//AIzaSyCwysRSZ0_YSK53LwmnmWbPjBxRFfXSy
    func sendInviteTo(token: String) {
        
        for user in self.users {
            if user[Constants.DataBase.deviceToken] as? String == token {
                    print("TOKEN: \(token)")
                    let url = URL(string: "https://fcm.googleapis.com/fcm/send")!
                    var request = URLRequest(url: url)
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.setValue("key=AAAAhcXRT-w:APA91bHWuRt8nzaGQFEVjrWrXJiAMKIeia6epWY09gjVP3_o2NRy38KXmROqY_TR1o6F0loJYbOE8EM6siN2zFPcq_Z5R0RI4arXAgnY_SPYAsf5mUInBo5Hv4NDbTmVegXMwGIVrHEY", forHTTPHeaderField: "Authorization")
                    request.httpMethod = "POST"
                    
                    var notData: [String: Any] = [
                        "to" : token,
                        "notification": [
                            "title" : "Video chat invite",
                            "body"  : "Click to accept",
                            //                icon  : "not icon"
                        ],
                        "data": [
                            //More notification data.
                            Constants.FCMData.notificationName: Constants.FCMData.sendChatInvite,
                            Constants.FCMData.name: "\(self.getDisplayName(email: (Auth.auth().currentUser?.email)!))",
                            Constants.FCMData.kApiKey: self.kApiKey,
                            Constants.FCMData.kSessionId: self.kSessionId,
                            Constants.FCMData.kToken: self.kToken,
                            Constants.FCMData.fromDevice: FCMTokenHelper.sharedInstance.currentDeviceTOken,
                        ]
                    ]
                    
                    request.httpBody = try? JSONSerialization.data(withJSONObject: notData, options: [])
                    let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        guard let data = data, error == nil else {                                                 // check for fundamental networking error
                            print("error=\(error)")
                            return
                        }
                        
                        if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                            print("statusCode should be 200, but is \(httpStatus.statusCode)")
                            print("response = \(response)")
                        }
                        
                        let responseString = String(data: data, encoding: .utf8)
                        print("responseString = \(responseString)")
                    }
                    task.resume()
                
            }
        }
        
        
    }
    
    //MARK: Table view Delegate Functions
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if users.count > 0 {
            print("UPDATED USERS: \(users)")
            let cell = tableView.dequeueReusableCell(withIdentifier: listCellIdentifier) as! UserListCell
            print("\( self.users[indexPath.row]["userName"])  \( self.users[indexPath.row]["online"])")
            cell.configureCell(userName:  self.users[indexPath.row][Constants.DataBase.userName] as! String, isOnline: (self.users[indexPath.row][Constants.DataBase.online] as! Int))
            return cell
        }
       return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }


    //MARK: IBActions
    @IBAction func logoutPressed(_ sender: Any) {
        if Auth.auth().currentUser != nil {
            do {
                FCMTokenHelper.sharedInstance.setOnlineTo(isOnline: false)
                try Auth.auth().signOut()
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                present(vc, animated: true, completion: nil)

                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
    }
    
    
    
}

