//
//  VideoCallViewController.swift
//  VideoChat
//
//  Created by Vidushi Jaiswal on 25/11/18.
//  Copyright © 2018 Vidushi Jaiswal. All rights reserved.
//

import UIKit
import OpenTok
import FirebaseAuth

class VideoCallViewController: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak var disconnectButton: UIButton!
    
    // Replace with your OpenTok API key
    var kApiKey = ""
    // Replace with your generated session ID
    var kSessionId = ""
    // Replace with your generated token
    var kToken = ""
    
     var session: OTSession?
     var publisher: OTPublisher?
    var subscriber: OTSubscriber?
    
    
    //MARK: Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()

               NotificationCenter.default.addObserver(self, selector: #selector(self.dismissVC), name: Notification.Name(Constants.NotificationObservers.dismissVC), object: nil)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.showAlert), name: Notification.Name(Constants.NotificationObservers.showAlert), object: nil)
        
        connectToAnOpenTokSession()
       
    }
    

    func connectToAnOpenTokSession() {
        session = OTSession(apiKey: kApiKey, sessionId: kSessionId, delegate: self)
        var error: OTError?
        session?.connect(withToken: kToken, error: &error)
        if error != nil {
            print(error!)
        }
        
    }
    
    @objc func showAlert() {
        let alertController = UIAlertController(title: "User declined Call", message: "Please try again after sometime", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func dismissVC() {
        FCMTokenHelper.sharedInstance.updateIsAvailableStatus(token: (Auth.auth().currentUser?.uid)!, value: true)
        self.dismiss(animated: true, completion: nil)
//

    }
    
    //MARK: Lifecycle functions
    @IBAction func disconnectPressed(_ sender: Any) {
        print("Disconnect pressed")
        var error: OTError?
        session?.disconnect(&error)
        
        self.dismissVC()
    }
    
    
}
// MARK: - OTSessionDelegate callbacks
extension VideoCallViewController: OTSessionDelegate {
    func sessionDidConnect(_ session: OTSession) {
        print("The client connected to the OpenTok session.")
        
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        guard let publisher = OTPublisher(delegate: self, settings: settings) else {
            return
        }
        
        var error: OTError?
        session.publish(publisher, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        guard let publisherView = publisher.view else {
            return
        }
        let screenBounds = UIScreen.main.bounds
        publisherView.frame = CGRect(x: screenBounds.width - 150 - 20, y: screenBounds.height - 150 - 20, width: 150, height: 150)
        view.addSubview(publisherView)
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("The client disconnected from the OpenTok session.")
        
//        FCMTokenHelper.sharedInstance.updateIsAvailableStatus(token: FCMTokenHelper.sharedInstance.senderToken, value: true)
//        
//        FCMTokenHelper.sharedInstance.updateIsAvailableStatus(token: FCMTokenHelper.sharedInstance.receiverToken, value: true)
        
        
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("The client failed to connect to the OpenTok session: \(error).")
    }
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("A stream was created in the session.")
        
        subscriber = OTSubscriber(stream: stream, delegate: self)
        guard let subscriber = subscriber else {
            return
        }
        
        var error: OTError?
        session.subscribe(subscriber, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        guard let subscriberView = subscriber.view else {
            return
        }
        subscriberView.frame = UIScreen.main.bounds
        view.insertSubview(subscriberView, at: 0)
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("A stream was destroyed in the session.")
    }
}
// MARK: - OTPublisherDelegate callbacks
extension VideoCallViewController: OTPublisherDelegate {
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("The publisher failed: \(error)")
    }
}


// MARK: - OTSubscriberDelegate callbacks
extension VideoCallViewController: OTSubscriberDelegate {
    public func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
        print("The subscriber did connect to the stream.")
    }
    
    public func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("The subscriber failed to connect to the stream.")
    }
    
}
