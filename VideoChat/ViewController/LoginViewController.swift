//
//  LoginViewController.swift
//  VideoChat
//
//  Created by Vidushi Jaiswal on 25/11/18.
//  Copyright © 2018 Vidushi Jaiswal. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase


class LoginViewController: UIViewController, UITextFieldDelegate {

    //MARK: Properties
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    var ref: DatabaseReference!
    var dbHandle: DatabaseHandle!
    
    
    //MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
         subscribeToKeyboardNotifications()
         hideKeyboardWhenTappedAround()
        
        userEmailTextField.delegate = self
        userNameTextField.delegate = self
        passwordTextField.delegate = self
        
        loginButton.layer.cornerRadius = 8
        loginButton.clipsToBounds = true
        
        signUpButton.layer.cornerRadius = 8
        signUpButton.clipsToBounds = true
    }


    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
         unsubscribeFromKeyboardNotifications()
        
    }
 
    //MARK: IBActions
    @IBAction func loginPressed(_ sender: Any) {
        Auth.auth().signIn(withEmail: userEmailTextField.text!, password: passwordTextField.text!) { (user, error) in
            if error == nil {
                print("Success \(String(describing: user))")
                
                let userID = Auth.auth().currentUser?.uid
                self.ref = Database.database().reference()
//                self.ref.child("users").child(userID!).setValue(["userName": "Anuj", "userEmail": self.userNameTextField.text, "online": false])
                
                //update the fcmtoken
                self.ref.child("users").child(userID!).updateChildValues(["deviceToken": FCMTokenHelper.sharedInstance.fcmToken]) {(error: Error?, ref: DatabaseReference) in
                    if let error = error {
                        print("Data could not be saved: \(error).")
                    } else {
                        print("Data saved successfully \(FCMTokenHelper.sharedInstance.fcmToken)!")
                        FCMTokenHelper.sharedInstance.setOnlineTo(isOnline: true)
                        self.getTokenOftheCurrentUser()
                        self.moveToUserList()
                    }
                }
//                self.ref.child("users").child(userID!).setValue(["deviceToken": FCMTokenHelper.sharedInstance.fcmToken]) {
//                    (error:Error?, ref:DatabaseReference) in
//                    if let error = error {
//                        print("Data could not be saved: \(error).")
//                    } else {
//                        print("Data saved successfully!")
//                    }
//                }
//
            } else {
                print("Error: \(String(describing: error))")
                let alertController = UIAlertController(title: "An error occured", message: "User name or password is incorrect. Please try again!", preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
    }
    
  
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        if userNameTextField.text == "" {
            let alertController = UIAlertController(title: "Error", message: "Please enter your email and password", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            present(alertController, animated: true, completion: nil)
            
        } else {
            
            Auth.auth().createUser(withEmail: userEmailTextField.text!, password: passwordTextField.text!) { (user, error) in
                
                if error == nil {
                    print("You have successfully signed up")
                    //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
                    
                    //save the user in database
                    let userID = Auth.auth().currentUser?.uid
                    print("userID: \(userID)")
                    self.ref = Database.database().reference()
                    
                    self.ref.child(Constants.DataBase.users).child(userID!).setValue([Constants.DataBase.userName: self.userNameTextField.text, Constants.DataBase.userEmail: self.userEmailTextField.text, Constants.DataBase.online: true, Constants.DataBase.isAvailable: true ,Constants.DataBase.deviceToken: FCMTokenHelper.sharedInstance.fcmToken], withCompletionBlock: {(error, ref) in
                        if let error = error {
                            print("Data could not be saved: \(error).")
                        } else {
                            print("Data saved successfully \(FCMTokenHelper.sharedInstance.fcmToken)!")
                            self.getTokenOftheCurrentUser()
                            
                            self.moveToUserList()
                        }
                    })
                    
                    
                    
                } else {
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    //MARK: Textfield Delegate Functions
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    //MARK: Helper Methods
    
    func getTokenOftheCurrentUser() {
        let userID = Auth.auth().currentUser?.uid
        
        ref.child(Constants.DataBase.users).child(userID!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let deviceToken = value?[Constants.DataBase.deviceToken] as? String ?? ""
            FCMTokenHelper.sharedInstance.currentDeviceTOken = deviceToken
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    

    
    func moveToUserList() {
        let userVC = storyboard!.instantiateViewController(withIdentifier: "UserListViewController") as! UserListViewController
        userVC.modalTransitionStyle = .crossDissolve
        self.present(userVC, animated: false, completion: nil)
    }
    
    func subscribeToKeyboardNotifications() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func unsubscribeFromKeyboardNotifications() {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        if userEmailTextField.isFirstResponder || passwordTextField.isFirstResponder
        {
            
            self.view.frame.origin.y =  (getKeyboardHieght(notification) - 150) * -1
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        
        if userEmailTextField.isFirstResponder || passwordTextField.isFirstResponder
        {
            self.view.frame.origin.y = 0
        }
    }
    
    
    func getKeyboardHieght(_ notification: Notification) -> CGFloat {
        
        let userInfo = notification.userInfo
        let keyboardSize = userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue
        return keyboardSize.cgRectValue.height
        
    }
    
}

