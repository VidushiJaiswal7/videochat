//
//  FCMTokenHelper.swift
//  VideoChat
//
//  Created by Vidushi Jaiswal on 26/11/18.
//  Copyright © 2018 Vidushi Jaiswal. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class FCMTokenHelper: NSObject{
    
     static let sharedInstance : FCMTokenHelper = FCMTokenHelper()
    
    var fcmToken: String = ""
    var currentDeviceTOken: String = ""
    
    var senderId: String = ""
    var receiverId: String = ""
    
    
    
    var ref: DatabaseReference!
    var dbHandle: DatabaseHandle!
    
    //Database functions
    func updateIsAvailableStatus(token: String, value: Bool) {
        
        self.ref = Database.database().reference()
        
        self.ref.child("users").child(token).updateChildValues(["isAvailable": value]) {(error: Error?, ref: DatabaseReference) in
            if let error = error {
                print("Data could not be saved: \(error).")
            } else {
                print("isAvailable status saved successfully")
            }
        }
    }
    
    func setOnlineTo(isOnline: Bool) {
        let userID = Auth.auth().currentUser?.uid
        self.ref = Database.database().reference()
        
        //update the online property
        self.ref.child("users").child(userID!).updateChildValues(["online": isOnline]) {(error: Error?, ref: DatabaseReference) in
            if let error = error {
                print("Data could not be saved: \(error).")
            } else {
                print("Data saved successfully")
            }
        }
    }
}


extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        /**
         Function to hide the keyboard when tapped anywhere else
         */
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
