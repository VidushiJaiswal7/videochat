//
//  Constants.swift
//  VideoChat
//
//  Created by Vidushi Jaiswal on 29/11/18.
//  Copyright © 2018 Vidushi Jaiswal. All rights reserved.
//

import Foundation

struct Constants {
    struct NotificationObservers {
        static let inviteToCallButton = "inviteToCallButton"
        static let showAlert = "showAlert"
        static let reloadData = "reloadData"
        static let dismissVC = "dismissVC"
    }
    
    struct DataBase {
        static let users = "users"
        static let userName = "userName"
        static let userEmail = "userEmail"
        static let online = "online"
        static let isAvailable = "isAvailable"
        static let deviceToken = "deviceToken"
    }
    
    struct FCMData {
        static let sender = "sender"
        static let userName = "userName"
        static let isAvailable = "isAvailable"
        static let deviceToken = "deviceToken"
        static let name = "name"
        static let kApiKey = "kApiKey"
        static let kSessionId = "kSessionId"
        static let kToken = "kToken"
        static let fromDevice = "fromDevice"
        static let apiKey = "apiKey"
        static let sessionId = "sessionId"
        static let token = "token"
        static let notificationName = "notificationName"
        static let decline = "decline"
        static let sendChatInvite = "sendChatInvite"
    }
}
