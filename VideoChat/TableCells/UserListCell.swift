//
//  UserListCell.swift
//  VideoChat
//
//  Created by Vidushi Jaiswal on 25/11/18.
//  Copyright © 2018 Vidushi Jaiswal. All rights reserved.
//

import Foundation
import UIKit

class UserListCell: UITableViewCell {
    @IBOutlet weak var onlineView: UIView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var inviteToCallButton: UIButton!
    
    
    @IBAction func inviteToCallButtonPressed(_ sender: Any) {
       
       
        let userInfo: [String : Any] = ["sender" : sender]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.NotificationObservers.inviteToCallButton), object: nil, userInfo: userInfo)
    }
    
    //65D737 - rgb(101,215,55)
    override func awakeFromNib() {
        onlineView.layer.cornerRadius = onlineView.frame.size.width/2
        onlineView.clipsToBounds = true
        inviteToCallButton.layer.cornerRadius = 8
        inviteToCallButton.clipsToBounds = true
    }
    
    func configureCell(userName: String, isOnline: Int) {
        userNameLabel.text = userName

        if isOnline == 1 {
            onlineView.backgroundColor = UIColor(red: 101/255, green: 215/255, blue: 55/255, alpha: 1.0)
            inviteToCallButton.isHidden = false
        } else {
            onlineView.backgroundColor = UIColor.gray
            inviteToCallButton.isHidden = true
        }
        
        
    }
}
